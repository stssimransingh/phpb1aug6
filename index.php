<?php 
include_once('connect.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css" />
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 mt-5 mx-auto p-5 bg-danger text-light">
                <h1>Add Question</h1>
                <hr/>
                <?php 
                    if(isset($_POST['question'])){
                        $question = $_POST['question'];
                        $optiona = $_POST['optiona'];
                        $optionb = $_POST['optionb'];
                        $optionc = $_POST['optionc'];
                        $optiond = $_POST['optiond'];
                        $answer = $_POST['answer'];

                        $qry = "INSERT INTO quiz (question,optiona, optionb, optionc, optiond, answer) VALUES ('$question','$optiona','$optionb','$optionc','$optiond','$answer')";
                        if($connect->query($qry)){
                            echo "<div class='alert alert-success'>Success</div>";
                        }else{
                            echo "<div class='alert alert-danger'>Error</div>";
                        }
                        
                    }
                ?>
                <form action="" method="post">
                <label for="">Question</label>
                <input type="text" name="question" class="form-control" />
                <label for="optiona">Option A</label>
                <input type="text" id="optiona" name="optiona" class="form-control" />
                <label for="">Option B</label>
                <input type="text" name="optionb" class="form-control" />
                <label for="">Option C</label>
                <input type="text" name="optionc" class="form-control" />
                <label for="">Option D</label>
                <input type="text" name="optiond" class="form-control" />
                <label for="">Answer</label>
                <select name="answer" class="form-control" id="">
                    <option>A</option>
                    <option>B</option>
                    <option>C</option>
                    <option>D</option>
                </select>
                <input type="submit" class="btn btn-success mt-3" />
                
                </form>
            </div>
        </div>
    </div>
</body>
</html>