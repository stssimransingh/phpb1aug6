<?php
include_once('connect.php');
if(isset($_GET['id'])){
    $id = $_GET['id'];
    $qry = "SELECT * FROM quiz WHERE id = '$id'";
    $result = $connect->query($qry);
    $res = $result->fetch_assoc();
}else{
    exit("You are not allowed.");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css" />
</head>
<body>
    <?php
        if(isset($_POST['question'])){
            $question = $_POST['question'];
            $optiona = $_POST['optiona'];
            $optionb = $_POST['optionb'];
            $optionc = $_POST['optionc'];
            $optiond = $_POST['optiond'];
            $answer1 = $_POST['answer'];
            $qry = "UPDATE quiz SET question = '$question', optiona = '$optiona', optionb = '$optionb', optionc = '$optionc', optiond = '$optiond', answer='$answer1' WHERE id = '$id'";
            if($connect->query($qry)){
                echo "Update";
                echo "<script>alert('Update $id');
                    window.location = 'showall.php';
                </script>";
                // header('Location: showall.php');
            }else{
                echo "ERRO".$connect->error;
            }
        }
    ?>
    <form action="" method="post">
        <input type="text" value="<?php echo $res['question']; ?>" placeholder="Question" name="question" />
        <input type="text" value="<?php echo $res['optiona']; ?>" placeholder="Option A" name="optiona" />
        <input type="text" value="<?php echo $res['optionb']; ?>" placeholder="Option B" name="optionb" />
        <input type="text" value="<?php echo $res['optionc']; ?>" placeholder="Option C" name="optionc" />
        <input type="text" value="<?php echo $res['optiond']; ?>"placeholder="Option D" name="optiond" />
        <?php 
            $answer = $res['answer'];
            $a = '';
            $b = '';
            $c = '';
            $d = '';
            if($answer == 'A'){
                $a = 'selected';
            }else if($answer == 'B'){
                $b = 'selected';
            }else if($answer == 'C'){
                $c = 'selected';
            }else if($answer == 'D'){
                $d = 'selected';
            }
        ?>
        <select name="answer" id="answer">

            <option <?php echo $a; ?>>A</option>
            <option <?php echo $b; ?>>B</option>
            <option <?php echo $c; ?>>C</option>
            <option <?php echo $d; ?>>D</option>
        </select>
        <input type="submit" />
    </form>
</body>
</html>