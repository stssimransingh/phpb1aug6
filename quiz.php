<?php include_once('connect.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css" />
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>QUIZ</h1>
                <hr/>
                <?php
                    $qry = "SELECT * FROM quiz";
                    $res = $connect->query($qry);
                    $i = 1;
                    while($row = $res->fetch_assoc()){
                        
                        ?>
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-warning">
                                Q. <?php echo $i; ?> <?php echo $row['question']; ?></li>
                                <li class="list-group-item">
                                A . <input type="radio" name="question<?php echo $row['id']; ?>" />
                                <?php echo $row['optiona']; ?></li>
                                <li class="list-group-item">
                                B . <input type="radio" name="question<?php echo $row['id']; ?>" />
                                <?php echo $row['optionb']; ?></li>
                                <li class="list-group-item">
                                C . <input type="radio" name="question<?php echo $row['id']; ?>" />
                                <?php echo $row['optionc']; ?></li>
                                <li class="list-group-item">
                                D . <input type="radio" name="question<?php echo $row['id']; ?>" />
                                <?php echo $row['optiond']; ?></li>
                                
                            </ul>

                        <?php
                        $i++;
                    }
                    
                ?>
                
            </div>
        </div>
    </div>
</body>
</html>