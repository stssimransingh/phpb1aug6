<?php 
include_once('connect.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css" />
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Question</th>
                            <th>Option A</th>
                            <th>Option B</th>
                            <th>Option C</th>
                            <th>Option D</th>
                            <th>Answer</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $qry = "SELECT * FROM quiz";
                            $res = $connect->query($qry);
                            $i = 1;
                            while($row = $res->fetch_assoc()){
                                echo "<tr>";
                                echo "<td>$i</td>";
                                echo "<td>$row[question]</td>";
                                echo "<td>$row[optiona]</td>";
                                echo "<td>$row[optionb]</td>";
                                echo "<td>$row[optionc]</td>";
                                echo "<td>$row[optiond]</td>";
                                echo "<td>$row[answer]</td>";
                                echo "<td>
                                <a href='edit.php?id=$row[id]' class='btn btn-warning'>Edit</a>
                                <button onclick='confirmdelete($row[id])' class='btn btn-danger'>Delete</button></td>";
                                echo "</tr>";
                                $i++;
                                //href='delete.php?id=$row[id]'
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function confirmdelete(id){
            if(confirm('Are you sure ?')){
                window.location = "delete.php?id="+id;
            }else{
                alert('Your request canceled');
            }
        }
    </script>
</body>
</html>